def decode_b64(value):
    result = 0
    for i in range(0, len(value)):
        result += ((ord(value[i]) - 0x40) << 6 * (len(value) - 1 - i));

    return result


def encode_b64(value, length=2):
    result = ""
    for i in range(0, length):
        sub_value = (value >> 6 * (length - 1 - i)) & 0x3f
        result += chr(sub_value + 0x40)

    return result


def decode_vl64(value):

    is_negative = (ord(value[0]) & 4) == 4
    total_bytes = (ord(value[0]) >> 3) & 7

    result = ord(value[0]) & 3
    shift_amount = 2

    for i in range(1, total_bytes):
        result |= (ord(value[i + 1]) & 0x3f) << shift_amount
        shift_amount += 6

    if is_negative:
        result = -result

    return result


def encode_vl64(value):
    vl_encoded = bytearray(6)

    byte_count = 1
    absolute_value = abs(value)

    vl_encoded[0] = (0x40 + (absolute_value & 3))

    if value < 0:
        vl_encoded[0] |= 4
    else:
        vl_encoded[0] |= 0

    absolute_value >>= 2

    while absolute_value != 0:
        byte_count += 1
        vl_encoded[byte_count] = (0x40 + (absolute_value & 0x3f))
        absolute_value >>= 6

    vl_encoded[0] |= byte_count << 3

    return vl_encoded.decode("utf8")[:byte_count]